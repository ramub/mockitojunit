@before, @After-- for each @test it will be called
@Beforeclass, @Afterclass -- for entire class once it will be called
we can use try catch --for exceptions 
otherwiese
@test(expected=NullPointerException.class)

udemy url for this course
https://github.com/in28minutes/MockitoTutorialForBeginners

assertEquals(2, todos.size());
assertThat(todos.size(), is(2));

List<String> list = mock(List.class);
Mockito.when(list.get(Mockito.anyInt())).thenReturn("in28Minutes");

given(list.get(Mockito.anyInt())).willReturn("in28Minutes");

List list = mock(List.class);
		Mockito.when(list.size()).thenReturn(10).thenReturn(20);
		assertEquals(10, list.size()); // First Call
		assertEquals(20, list.size()); // Second Call

@Test(expected = RuntimeException.class)
	public void letsMockListGetToThrowException() {
		List<String> list = mock(List.class);
		when(list.get(Mockito.anyInt())).thenThrow(
				new RuntimeException("Something went wrong"));
		list.get(0);
	}

@RunWith(MockitoJUnitRunner.class)
public class TodoBusinessImplMockitoInjectMocksTest {
	@Mock
	TodoService todoService;

	@InjectMocks
	TodoBusinessImpl todoBusinessImpl;



Spy:
i want to override specific methods then use Spy
it is like a creating a new arraylist and you can stub certain methods. its like partial overriding.

@Test
	public void creatingASpyOnArrayList_overridingSpecificMethods() {
		List<String> listSpy = spy(ArrayList.class);
		listSpy.add("Ranga");
		listSpy.add("in28Minutes");

		stub(listSpy.size()).toReturn(-1);

		assertEquals(-1, listSpy.size());
		assertEquals("Ranga", listSpy.get(0));

		stub(listSpy.size()).toReturn(-1);


why does mockito not allow stubbing final and private methods

PowerMockito with mockito 

@RunWith(PowerMockRunner.class)
@PrepareForTest({ UtilityClass.class})

PowerMockito.mockStatic(UtilityClass.class);
when(UtilityClass.staticMethod(anyLong())).thenReturn(150);

PowerMockito.verifyStatic();
UtilityClass.staticMethod(1 + 2 + 3);


invoking private methods

long value = (Long) Whitebox.invokeMethod(systemUnderTest,
				"privateMethodUnderTest");
